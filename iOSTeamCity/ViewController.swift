//
//  ViewController.swift
//  iOSTeamCity
//
//  Created by Reza Dehnavi on 8/26/20.
//  Copyright © 2020 Reza Dehnavi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let targetName = Bundle.main.infoDictionary?["TargetName"] as? String ?? "iOSTeamCity"
        self.view.backgroundColor = targetName == "iOSTeamCity" ? .green : .red
    }


}

