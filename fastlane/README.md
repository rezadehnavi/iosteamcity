fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios release
```
fastlane ios release
```

### ios create_ipa
```
fastlane ios create_ipa
```

### ios nightly_build
```
fastlane ios nightly_build
```

### ios nightly_build_all
```
fastlane ios nightly_build_all
```

### ios upload_to_server
```
fastlane ios upload_to_server
```

### ios post_on_discord
```
fastlane ios post_on_discord
```

### ios add_badge_for_beta
```
fastlane ios add_badge_for_beta
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
